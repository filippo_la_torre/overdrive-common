package space.overdrive.common.image;


import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;

/**
 * Created by filippolatorre on 16/02/17.
 */
public class ImageUtility {

    private static final int LIMIT_CHAR = 15;
    private static final int X = 40;

    public BufferedImage createImage(String text){
        BufferedImage off_Image =
                new BufferedImage(500, 300,
                        BufferedImage.TYPE_INT_ARGB);

        Graphics2D g2 = off_Image.createGraphics();

        //g2.fillRect ( 0, 0, off_Image.getWidth(), off_Image.getHeight() );
        g2.setBackground(Color.GREEN);
        g2.clearRect(0,0,off_Image.getWidth(), off_Image.getHeight());
        Font font;
        try {
            InputStream fontStream = getClass().getClassLoader().getResourceAsStream("font/OpenSansEmoji.otf");
            font = Font.createFont(Font.TRUETYPE_FONT, fontStream);
            font = font.deriveFont(18f);
        }catch (FontFormatException e) {
            font = new Font("OpenSansEmoji", Font.BOLD, 20);
        } catch (IOException e) {
            font = new Font("OpenSansEmoji", Font.BOLD, 20);
        }
        g2.setFont(font);
        int y = 80;
        String line = "";
        if(text.length() > LIMIT_CHAR){
            String[] words = text.split(" ");
            int character = 0;
            for (String word:
                 words) {
                character = character + word.length() + 1;
                if(character <= LIMIT_CHAR){
                    line = line.concat(word).concat(" ");
                }else{
                    if(word.length() > 10){
                        byte ptext[] = new byte[0];
                        String value;
                        try {
                            ptext = line.getBytes("UTF-8");
                            value = new String(ptext, "UTF-8");
                        } catch (UnsupportedEncodingException e) {
                            value = line;
                        }
                        g2.drawString(value,  X, y);
                        y = y + 40;
                        try {
                            ptext = word.getBytes("UTF-8");
                            word = new String(ptext, "UTF-8");
                        } catch (UnsupportedEncodingException e) {
                            word = line;
                        }
                        g2.drawString(word,  X, y);
                        y = y + 40;
                        character = 0;
                        line = "";
                    }else{
                        line = line.concat(word).concat(" ");
                        byte ptext[] = new byte[0];
                        String value;
                        try {
                            ptext = line.getBytes("UTF-8");
                            value = new String(ptext, "UTF-8");
                        } catch (UnsupportedEncodingException e) {
                            value = line;
                        }
                        g2.drawString(value,  X, y);
                        y = y + 40;
                        character = 0;
                        line = "";
                    }
                }
            }
        }else{
            line = text;
        }
        byte ptext[] = new byte[0];
        String value;
        try {
            ptext = line.getBytes("UTF-8");
            value = new String(ptext, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            value = line;
        }
        g2.drawString(value, X, y);

        return off_Image;
    }
}
