package space.overdrive.common.image;

import org.junit.Test;

import java.awt.image.BufferedImage;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.*;

/**
 * Created by filippolatorre on 06/03/17.
 */
public class ImageUtilityTest {

    @Test
    public void createImage() throws Exception {
        ImageUtility imageUtility = new ImageUtility();
        BufferedImage result = imageUtility.createImage("First test of test");
        assertThat(result.getHeight(),equalTo(300));
    }

}